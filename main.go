/*
Copyright © 2023 xiaoshangyangcode
*/
package main

import "gitee.com/xiaoshanyangcode/postfixlogparse/cmd"

func main() {
	cmd.Execute()
}
